package web.controleurs;

import dao.Dao;
import entites.Client;
import entites.Region;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;

import javax.inject.Inject;
import javax.inject.Named;
import web.tableassoc.TableAssocClients;
import web.tableassoc.TableAssocRegions;

@Named
@SessionScoped
public class ControleurCrudClient implements Serializable{ 
    
    @Inject private Dao                        dao;
    @Inject private ControleurSelectionClient  contSelClient;
    @Inject private TableAssocRegions          tRegions;  
    @Inject private TableAssocClients          tClients;  
    
    private List<Region>                       lesRegions;
         
    private Client nouveauClient;
   
    @PostConstruct
    public void init(){ lesRegions=tRegions.getListe();}
        
    public void  enregisterModifications(){  dao.repercuterMAJ(getClientSel()); }
    
    public void  supprimerLeClient(){
      
       dao.supprimerEntite(getClientSel());
       
       tClients.supprimer(getClientSel());
       setClientSel(getLesClients().get(0));   
    }
    
    public void ajouterClient(){
    
       nouveauClient= new Client();
       nouveauClient.setLaRegion(lesRegions.get(0));
       nouveauClient.setNomCli("Saisir le nom");
       nouveauClient.setAdrCli("Saisir la ville");
         
       dao.enregistrerEntite(nouveauClient);
       
       tClients.ajouter(nouveauClient);
       
       int indexNouvCli=getLesClients().indexOf(nouveauClient);
       setClientSel(getLesClients().get(indexNouvCli));
        
    }
          
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    
    
   
     public List<Region> getLesRegions() {
        return lesRegions;
    }

    public void setLesRegions(List<Region> lesRegions) {
        this.lesRegions = lesRegions;
    }
    
    public ControleurSelectionClient getContSelClient() {
        return contSelClient;
    }

    public void setContSelClient(ControleurSelectionClient contSelClient) {
        this.contSelClient = contSelClient;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS et SETTERS DELEGUES">
    
    public Client getClientSel() {
        return contSelClient.getClientSel();
    }
    
    public void setClientSel(Client client) {
        contSelClient.setClientSel(client);
    }
    
    public List<Client> getLesClients(){
    
      return contSelClient.getLesClients();
    }
    
    //</editor-fold>
}


