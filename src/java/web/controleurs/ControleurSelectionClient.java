package web.controleurs;

import entites.Client;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;

import javax.inject.Inject;
import javax.inject.Named;
import web.tableassoc.TableAssocClients;

@Named
@SessionScoped
public class ControleurSelectionClient implements Serializable{ 
       
    @Inject private TableAssocClients tClients;
      
    private List<Client>              lesClients;
    private Client                    clientSel;
    
    @PostConstruct
    public void init(){
       
        lesClients=tClients.getListe();
        if (! lesClients.isEmpty()) clientSel=lesClients.get(0);   
    } 

    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    
    public List<Client> getLesClients() {
        return lesClients;
    }

    public void setLesClients(List<Client> lesClients) {
        this.lesClients = lesClients;
    }

    public Client getClientSel() {
        return clientSel;
    }

    public void setClientSel(Client clientSel) {
        this.clientSel = clientSel;
    }

    
        //</editor-fold>
}




