package web.controleurs;

import bal.client.BalClient;
import bal.commande.BalCommande;
import entites.Client;
import entites.Commande;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class ControleurCommandesParClient implements Serializable{
    
    @Inject private ControleurSelectionClient contComboSelCli;
    
    @Inject private BalCommande               balCommande;
    @Inject private BalClient                 balClient;     
  
    private Commande                          commandeSel;  
    private String                            messageTableau;
  
    public void prevalide(ComponentSystemEvent e){ this.messageTableau=null;} 
    
    @PostConstruct
    public void  init(){ traiterSelectionCombo();}
    
    public void  traiterSelectionCombo(){ if (this.getClientSel()!=null)messageTableau="Pas de commande pour ce client"; }
   
    public Float calculMontantCommandeHt(Commande cmd){ return balCommande.montantCommandeHT(cmd);}
    
    public Float calculMontantCommandeTTC(Commande cmd){ return balCommande.montantCommandeTTC(cmd);}
    
    public Float calculCaClient(){return balClient.caAnneeEnCours(this.getClientSel()); }
    
    public Float calculSoldeClient(){ return -balClient.resteARegler(this.getClientSel()); }
    
    public void  afficherCommande() throws IOException{
      
      FacesMessage msg = new FacesMessage("Commande Sélectionnée",commandeSel.getNumCom().toString());
      FacesContext.getCurrentInstance().addMessage(null, msg);
      //   FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");  
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    
    public String getMessageTableau() {
        return messageTableau;
    }

    public void setMessageTableau(String messageTableau) {
        this.messageTableau = messageTableau;
    }
    
     public Commande getCommandeSel() {
        return commandeSel;
    }

    public void setCommandeSel(Commande commandeSel) {
        this.commandeSel = commandeSel;
    }
       
    //</editor-fold>
 
    //<editor-fold defaultstate="collapsed" desc="GETTERS SETTERS DELEGUES">
    
    public Client getClientSel() {
        return contComboSelCli.getClientSel();
    }
    
    public void setClientSel(Client clientSel) {
        
        contComboSelCli.setClientSel(clientSel);
        
    }
    //</editor-fold>
}


