
package dao;

public interface Dao {
    
   void enregistrerEntite (Object entite);
   void supprimerEntite  (Object entite);
   void repercuterMAJ     (Object entite);
   
   void detache(Object entite);
   
   void rafraichir(Object entite);
    
}
